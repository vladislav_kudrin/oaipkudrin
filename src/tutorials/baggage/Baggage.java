package tutorials.baggage;

public class Baggage {
    private String surname;
    private int space;
    private double weight;

    public Baggage(String surname, int space, double weight) {
        this.surname = surname;
        this.space = space;
        this.weight = weight;
    }

    public Baggage() {
        this("Unknown", 0, 0);
    }

    public static boolean defineHandLuggage(Baggage baggage) {
        if(baggage.space == 1 && baggage.weight < 11) {
            return true;
        }

        return false;
    }

    public String isSurname() {
        return surname;
    }
}

