package tutorials.representation_of_numbers;

import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);

        System.out.print("Please, enter your number: ");

        System.out.println("Representation as a binary: " + Representation.representAsBinary(input.nextDouble()));
    }
}
