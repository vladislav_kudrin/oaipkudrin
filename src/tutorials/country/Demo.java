package tutorials.country;

import java.util.Scanner;

public class Demo {
    public static Scanner input = new Scanner(System.in);

    public static void main(String[] args) {

        System.out.print("Please, enter a number of countries: ");
        Country[] countries = new Country[input.nextInt()];

        inputCharacteristics(countries);

        System.out.println("Country with a maximal density: " + countries[calculateMaxDensity(countries)]);
    }

    private static void inputCharacteristics(Country[] countries) {
        String name;
        String capital;
        double square;
        int population;

        for(int index = 0; index < countries.length; index++) {
            System.out.println("\nPlease, enter characteristics for a country " + (index + 1));
            System.out.print("Please, enter a country's name: ");
            input.nextLine();
            name = input.nextLine();
            System.out.print("Please, enter a country's capital: ");
            capital = input.nextLine();
            System.out.print("Please, enter a country's square as metres^2: ");
            square = input.nextDouble();
            System.out.print("Please, enter a country's population: ");
            population = input.nextInt();

            countries[index] = new Country(name, capital, square, population);
        }
    }

    private static int calculateMaxDensity(Country[] countries) {
        int maxDensity = 0;
        for(int index = 1; index < countries.length; index++) {
            if(countries[index].density() > countries[maxDensity].density()) {
                maxDensity = index;
            }
        }

        return maxDensity;
    }
}