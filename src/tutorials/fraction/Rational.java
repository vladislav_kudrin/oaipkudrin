package tutorials.fraction;

public class Rational {
    private int numerator;
    private int denominator;

    public Rational(int numerator, int denominator) {
        this.numerator = numerator;
        this.denominator = denominator;
    }

    public Rational() {
        this(0, 0);
    }

    public void setNumerator(int numerator) {
        this.numerator = numerator;
    }

    public void setDenominator(int denominator) {
        this.denominator = denominator;
    }

    public int getDenominator() {
        return denominator;
    }

    public int getNumerator() {
        return numerator;
    }

    public Rational add(Rational fraction) {
        if(denominator == fraction.getDenominator()) {
            return new Rational(numerator + fraction.getNumerator(), denominator);
        }

        return new Rational(numerator * fraction.getDenominator() + fraction.getNumerator() * denominator, denominator * fraction.getDenominator());
    }

    public Rational subtract(Rational fraction) {
        if(denominator == fraction.getDenominator()) {
            return new Rational(numerator - fraction.getNumerator(), denominator);
        }

        return new Rational(numerator * fraction.getDenominator() - fraction.getNumerator() * denominator, denominator * fraction.getDenominator());
    }

    public Rational multiply(Rational fraction) {

        return new Rational(numerator * fraction.getNumerator(), denominator * fraction.getDenominator());
    }

    public Rational divide(Rational fraction) {
        if(numerator < 0 && fraction.getNumerator() < 0) {
            return  new Rational(Math.abs(numerator) * fraction.getDenominator(), denominator * Math.abs(fraction.getNumerator()));
        }

        return new Rational(numerator * fraction.getDenominator(), denominator * fraction.getNumerator());
    }

    public Rational reduction() {
        int tempNumerator = Math.abs(numerator);
        int tempDenominator = denominator;
        int commonDenominator = 1;

        while(tempNumerator % tempDenominator != 0) {
            commonDenominator = tempNumerator % tempDenominator;
            tempNumerator = tempDenominator;
            tempDenominator = commonDenominator;
        }

        return new Rational(numerator / commonDenominator, denominator / commonDenominator);
    }

    public String toString() {
        return (numerator == 0) ? "0" : numerator + "/" + denominator;
    }
}