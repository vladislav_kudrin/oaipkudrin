package tutorials.robot;

public enum Direction {
    UP,
    RIGHT,
    DOWN,
    LEFT;
}