package tutorials.book;

/**
 * A class to store constructors and compareYear method.
 *
 * @author nxfh
 */
public class Book {
    private String surname;
    private String bookName;
    private int year;

    public Book(String surname, String bookName, int year) {
        this.surname = surname;
        this.bookName = bookName;
        this.year = year;
    }

    public Book() {
        this("Unknown", "Unknown", 0);
    }

    public int getYear() {
        return year;
    }

    public String toString() {
        return "Book {" +
                "author's surname: " + surname +
                ", name of a book: " + bookName +
                ", year of publication: " + year +
                '}';
    }

    /**
     * A method to compare years of a publication.
     *
     * @param book the
     * @return the comparison's result.
     */
    public boolean compareYear(Book book) {
        return getYear() == book.getYear();
    }
}