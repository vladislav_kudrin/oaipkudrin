package tutorials.christmas_tree;

import java.util.Scanner;

/**
 *
 *
 *
 *
 * @author nxfh
 */
public class Main {
    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);
        double height, balance;
        boolean natural;

        System.out.print("Please, enter your balance limit: ");
        balance = input.nextDouble();
        System.out.print("Please, enter height in metres as you prefer: ");
        height = input.nextDouble();
        System.out.println("Do you prefer a natural christmas tree?");
        input.nextLine();

        if(input.nextLine().toLowerCase().equals("yes")) {
            natural = true;
        }
        else {
            natural = false;
        }

        ChristmasTree christmasTree = new ChristmasTree(height, natural);

        System.out.println("\nYour order: \n" + christmasTree);
        System.out.printf("Price: %5.2f RUB\n", christmasTree.calculatePrice());

        System.out.println("\nRecommended for your balance: ");
        ChristmasTree validNaturalChristmasTree = ChristmasTree.showValidNatural(balance);
        System.out.printf("Natural: %5.2f metres\n", validNaturalChristmasTree.getHeight());
        ChristmasTree validNotNaturalChristmasTree = ChristmasTree.showValidNotNatural(balance);
        System.out.printf("Not natural: %5.2f metres", validNotNaturalChristmasTree.getHeight());
    }
}