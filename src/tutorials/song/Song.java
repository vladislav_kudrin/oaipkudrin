package tutorials.song;

public class Song {
    private String name;
    private String performer;
    private int seconds;

    public Song(String name, String performer, int seconds) {
        this.name = name;
        this.performer = performer;
        this.seconds = seconds;
    }

    public Song() {
        this("Unknown", "Unknown", 0);
    }

    public int getSeconds() {
        return seconds;
    }

    public static String category(Song songs) {
        if (songs.seconds < 120) {
            return "short";
        }
        else if (songs.seconds > 120 && songs.seconds < 240) {
            return "medium";
        }

        return "long";
    }

    public static void findShortSongs(Song[] songs) {
        int counter = 0;

        System.out.println("\nShort songs:");

        for(int index = 0; index < songs.length; index++) {
            if(category(songs[index]).equals("short")) {
                System.out.println(songs[index]);
                counter++;
            }
        }

        if(counter < 1) {
            System.out.println("Not found");
        }
    }

    public static boolean compareCategories(Song firstSong, Song lastSong) {
        if(category(firstSong).equals(category(lastSong))) {
            return true;
        }

        return false;
    }

    public String toString() {
        return "Song {" +
                "song's name: " + name +
                ", performer: " + performer +
                ", seconds: " + seconds +
                '}';
    }
}