package tutorials.fraction;

import java.util.Scanner;

public class Main {
    public static Rational firstRational = new Rational();
    public static Rational secondRational = new Rational();

    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);
        Rational result;

        String expression, firstFraction, secondFraction;

        System.out.println("Please, enter an expression as");
        System.out.println("'first_numerator/first_denominator operator second_numerator/second_denominator'");
        System.out.println("\nValid operators:");
        System.out.println("+ addition\n- subtraction\n* multiplication\n/ division\n");
        expression = input.nextLine();

        String[] variables = expression.split(" ");
        firstFraction = String.valueOf(variables[0]);
        secondFraction = String.valueOf(variables[2]);

        createFractions(firstFraction, secondFraction);

        if(firstRational.getDenominator() == 0 || secondRational.getDenominator() == 0) {
            System.out.println("Denominators cannot be equal to 0!");
            return;
        }

        firstRational = firstRational.reduction();
        secondRational = secondRational.reduction();

        switch(variables[1]) {
            case "+":
                result = firstRational.add(secondRational).reduction();
                break;
            case "-":
                result = firstRational.subtract(secondRational).reduction();
                break;
            case "*":
                result = firstRational.multiply(secondRational).reduction();
                break;
            case "/":
                result = firstRational.divide(secondRational).reduction();
                break;
            default:
                System.out.println("You entered an invalid operator.");
                return;
        }

        System.out.println(expression + " = " + result);
    }

    private static void createFractions(String firstFraction, String secondFraction) {
        int firstNumerator, secondNumerator;
        int firstDenominator, secondDenominator;

        String[] variables = firstFraction.split("/");
        firstNumerator = Integer.valueOf(variables[0]);
        firstDenominator = Integer.valueOf(variables[1]);
        firstRational = new Rational(firstNumerator, firstDenominator);

        variables = secondFraction.split("/");
        secondNumerator = Integer.valueOf(variables[0]);
        secondDenominator = Integer.valueOf(variables[1]);
        secondRational = new Rational(secondNumerator, secondDenominator);
    }
}