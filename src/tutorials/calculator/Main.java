package tutorials.calculator;

import java.util.Scanner;

/**
 * A class to calculate.
 *
 * @author nxfh
 */
public class Main {
    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);

        String expression;
        int firstNumber, secondNumber;

        System.out.println("Please, enter an expression as 'first_operand operator second_operand'");
        System.out.println("\nValid operators:");
        System.out.println("+ addition\n- subtraction\n* multiplication\n/ division\n% remainder\n^ power\n");
        expression = input.nextLine();

        String[] variables = expression.split(" ");
        firstNumber = Integer.valueOf(variables[0]);
        secondNumber = Integer.valueOf(variables[2]);

        switch(variables[1]) {
            case "+":
                System.out.println("\n" + expression + " = " + MathInt.add(firstNumber, secondNumber));
                break;
            case "-":
                System.out.println("\n" + expression + " = " + MathInt.subtract(firstNumber, secondNumber));
                break;
            case "*":
                System.out.println("\n" + expression + " = " + MathInt.multiply(firstNumber, secondNumber));
                break;
            case "/":
                System.out.println("\n" + expression + " = " + MathInt.divide(firstNumber, secondNumber));
                break;
            case "%":
                System.out.println("\n" + expression + " = " + MathInt.remainder(firstNumber, secondNumber));
                break;
            case "^":
                System.out.println("\n" + expression + " = " + MathInt.power(firstNumber, secondNumber));
                break;
        }
    }
}