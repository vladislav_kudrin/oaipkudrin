package tutorials.college;

import java.util.ArrayList;

public class Teacher extends Person{
    private String object;
    private boolean curator;

    Teacher(String surname, Gender gender, String object, boolean curator) {
        super(surname, gender);
        this.object = object;
        this.curator = curator;
    }

    public static void showCurators(ArrayList<Teacher> teachers) {
        for(int index = 0; index < teachers.size(); index++) {
            if(teachers.get(index).curator) {
                System.out.println(teachers.get(index));
            }
        }
    }

    public String toString() {
        return isSurname() + " {gender: " +
                getGender() + ", object: " +
                object + ", curator: " +
                curator + '}';
    }
}
