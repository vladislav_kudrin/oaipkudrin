package tutorials.palindrome;
import java.util.Scanner;

public class Palindrome {
    public static void main(String[] args){
        Scanner input = new Scanner(System.in);

        System.out.print("Please, enter a phrase or a word: ");
        String string = input.nextLine().replaceAll("[- ,.:;(){}\'\"!?]", "").toLowerCase();

        comparison(string);
    }

    private static void comparison(String string) {
        String reversedString = new StringBuffer(string).reverse().toString();

        if(reversedString.equals(string)) {
            System.out.println("It's a palindrome.");
        }
        else {
            System.out.println("It's not a palindrome.");
        }
    }
}