package tutorials.sea_battle;

import java.util.ArrayList;

public class Ship {
    private ArrayList<Integer> location;

    public Ship(int startCoord, int shipLength) {
        location = new ArrayList<>();
        for (int index = startCoord; index < startCoord + shipLength; index++) {
            location.add(index);
        }
    }

    public String shoot(int coord) {
        String result;

        if(location.contains(coord)) {
            location.remove(new Integer(coord));

            if(location.isEmpty()) {
                result = "You won! Ship is defeated and drowned!";
            }
            else {
                result = "Hit!";
            }
        }
        else {
            result = "Miss...";
        }

        return result;
    }

    public String toString() {
        return "Location " + location;
    }
}