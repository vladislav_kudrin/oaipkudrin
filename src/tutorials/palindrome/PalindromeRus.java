package tutorials.palindrome;
import java.util.Scanner;

public class PalindromeRus {
    public static void main(String[] args){
        Scanner input = new Scanner(System.in);

        System.out.print("Пожалуйста, введите фразу или слово: ");
        String string = input.nextLine().replaceAll("[- ,.:;(){}\'\"!?]", "").toLowerCase();

        comparison(string);
    }

    private static void comparison(String string) {
        String reversedString = new StringBuffer(string).reverse().toString();

        if(reversedString.equals(string) == true) {
            System.out.println("Палиндром");
        }
        else {
            System.out.println("Не палиндром");
        }
    }
}