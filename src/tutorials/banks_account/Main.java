package tutorials.banks_account;

import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);
        double balance, percent;

        System.out.print("Please, enter your current balance in RUB: ");
        balance = input.nextDouble();
        System.out.print("Please, enter an annual interest rate: ");
        percent = input.nextDouble();

        Account account = new Account(balance, percent);

        System.out.println(account);
        System.out.printf("\nBalance includes an annual interest charge for the year: %5.2f RUB\n", account.calculateAnnualInterestCharge());
    }
}