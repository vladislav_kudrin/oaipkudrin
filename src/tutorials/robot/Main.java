package tutorials.robot;

import java.util.Scanner;

public class Main {
    public static Scanner input = new Scanner(System.in);

    public static void main(String[] args) {
        Robot robot = new Robot(0, 0, Direction.UP);

        int destinationX, destinationY;

        System.out.println("Please, enter a destination for a robot");
        System.out.print("Coordinate X: ");
        destinationX = input.nextInt();
        System.out.print("Coordinate Y: ");
        destinationY = input.nextInt();

        move(destinationX, destinationY, robot);

        System.out.println("The robot is located at coordinates " + robot.getCoordX() + " by x and " + robot.getCoordY() + " by y.");
    }

    private static void move(int destinationX, int destinationY, Robot robot) {
        if(destinationX < robot.getCoordX()) {
            while(robot.getDirection() != Direction.LEFT) {
                turnLeft(robot);
            }

            while(robot.getCoordX() != destinationX) {
                step(robot);
            }
        }
        else if(destinationX > robot.getCoordX()) {
            while(robot.getDirection() != Direction.RIGHT) {
                turnRight(robot);
            }

            while(robot.getCoordX() != destinationX) {
                step(robot);
            }
        }

        if(destinationY < robot.getCoordY()) {
            while(robot.getDirection() != Direction.DOWN) {
                turnLeft(robot);
            }

            while(robot.getCoordY() != destinationY) {
                step(robot);
            }
        }
        else if(destinationY > robot.getCoordY()) {
            while(robot.getDirection() != Direction.UP) {
                turnRight(robot);
            }

            while(robot.getCoordY() != destinationY) {
                step(robot);
            }
        }
    }

    private static void turnLeft(Robot robot) {
        switch(robot.getDirection()) {
            case UP:
                robot.setDirection(Direction.LEFT);
                break;
            case LEFT:
                robot.setDirection(Direction.DOWN);
                break;
            case DOWN:
                robot.setDirection(Direction.RIGHT);
                break;
            case RIGHT:
                robot.setDirection(Direction.UP);
                break;
        }
    }

    private static void turnRight(Robot robot) {
        switch(robot.getDirection()) {
            case UP:
                robot.setDirection(Direction.RIGHT);
                break;
            case RIGHT:
                robot.setDirection(Direction.DOWN);
                break;
            case DOWN:
                robot.setDirection(Direction.LEFT);
                break;
            case LEFT:
                robot.setDirection(Direction.UP);
                break;
        }
    }

    private static void step(Robot robot) {
        switch(robot.getDirection()) {
            case LEFT:
                robot.setCoordX(robot.getCoordX() - 1);
                break;
            case RIGHT:
                robot.setCoordX(robot.getCoordX() + 1);
                break;
            case DOWN:
                robot.setCoordY(robot.getCoordY() - 1);
                break;
            case UP:
                robot.setCoordY(robot.getCoordY() + 1);
                break;
        }
    }
}