package tutorials.book;

import java.util.Scanner;

/**
 * A class to show results.
 *
 * @author nxfh
 */
public class Main {
    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);

        int firstIndex = 0, secondIndex = 0;
        Book[] books = new Book[5];

        books[0] = new Book("Surname", "First book", 2015);
        books[1] = new Book("Surname", "Second book", 2016);
        books[2] = new Book("Surname", "Third book", 2017);
        books[3] = new Book("Surname", "Fourth book: part 1", 2018);
        books[4] = new Book("Surname", "Fourth book: part 2", 2018);

        showInformation(books);
        System.out.println("");

        System.out.print("Please, enter a first book's number: ");
        firstIndex = input.nextInt();
        System.out.print("Please, enter a second book's number: ");
        secondIndex = input.nextInt();

//        System.out.print("Publication's year of a first book " + (Book.compareYear(books[firstIndex - 1],books[secondIndex - 1])?"is ":"isn't "));
        if(books[firstIndex - 1].compareYear(books[secondIndex - 1])) {
            System.out.println("Publication's year of a first book is equal to publication's year of a second book.");
        }
        else {
            System.out.println("Publication's year of a first book isn't equal to publication's year of a second book.");
        }
    }

    /**
     * A method returns books written in 2018.
     *
     * @param books array of books.
     */
    private static void showInformation(Book[] books) {
        int counter = 0;

        System.out.println("\nBooks written in 2018:");

        for(int index = 0; index < books.length; index++) {
            if(books[index].getYear() == 2018) {
                System.out.println(books[index]);
                counter++;
            }
        }

        if(counter < 1) {
            System.out.println("Not found");
        }
    }
}