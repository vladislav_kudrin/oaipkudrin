package tutorials.college;

import java.util.ArrayList;

public class Person {
    private String surname;
    private Gender gender;

    public Person(String surname, Gender gender) {
        this.surname = surname;
        this.gender = gender;
    }

    public String isSurname() {
        return surname;
    }

    public Gender getGender() {
        return gender;
    }

    public static void showMales(ArrayList<Person> persons) {
        for(int index = 0; index < persons.size(); index++) {
            if(persons.get(index).gender.equals(Gender.MALE)) {
                System.out.println(persons.get(index));
            }
        }
    }

    public String toString() {
        return surname + "{ gender: " +
                gender +
                '}';
    }
}
