package tutorials.representation_of_numbers;

public class Representation {
    public static String representAsBinary(double number) {
        String result;

        if(number % 1 == 0) {
            result = Integer.toBinaryString((int) number);
        }
        else {
            long numberBits = Double.doubleToLongBits(number);
            result = ((number > 0) ? "0" + Long.toBinaryString(numberBits) : Long.toBinaryString(numberBits));
        }

        return result;
    }
}