package tutorials.college;

import java.util.ArrayList;

public class Student extends Person{
    private int year;
    private String code;

    Student(String surname, Gender gender, int year, String code) {
        super(surname, gender);
        this.year = year;
        this.code = code;
    }

    public static void showFemales(ArrayList<Student> students) {
        for(int index = 0; index < students.size(); index++) {
            if(students.get(index).getGender().equals(Gender.FEMALE)&&students.get(index).year == 2017) {
                System.out.println(students.get(index));
            }
        }
    }

    public String toString() {
        return isSurname() + " {gender: " +
                getGender() + ", enrollment's year: " +
                year + ", code of object: " +
                code + '}';
    }
}
