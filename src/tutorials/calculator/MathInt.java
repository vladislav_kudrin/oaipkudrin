package tutorials.calculator;

/**
 * A class to store methods.
 *
 * @author nxfh
 */
public class MathInt {
    /**
     * A method returns an addition's result.
     *
     * @param firstNumber first operand.
     * @param secondNumber second operand.
     * @return the addition's result.
     */
    public static int add(int firstNumber, int secondNumber) {
        return firstNumber + secondNumber;
    }

    /**
     * A method returns a subtraction's result.
     *
     * @param firstNumber first operand.
     * @param secondNumber second operand.
     * @return the subtraction's result.
     */
    public static int subtract(int firstNumber, int secondNumber) {
        return firstNumber - secondNumber;
    }

    /**
     * A method returns a division's result.
     *
     * @param firstNumber first operand.
     * @param secondNumber second operand.
     * @return the division's result.
     */
    public static double divide(double firstNumber, double secondNumber) {
        if(secondNumber == 0) {
            System.out.println("You cannot divide by zero!");

            return 0;
        }

        return firstNumber / secondNumber;
    }

    /**
     * A method returns a multiplication's result.
     *
     * @param firstNumber first operand.
     * @param secondNumber second operand.
     * @return the multiplication's result.
     */
    public static int multiply(int firstNumber, int secondNumber) {
        return firstNumber * secondNumber;
    }

    /**
     * A method returns a remainder's result.
     *
     * @param firstNumber first operand.
     * @param secondNumber second operand.
     * @return the remainder's result.
     */
    public static int remainder(int firstNumber, int secondNumber) {
        if(secondNumber == 0) {
            System.out.println("You cannot divide by zero!");
            return 0;
        }

        return firstNumber % secondNumber;
    }

    /**
     * A method returns a power's result.
     *
     * @param firstNumber first operand.
     * @param secondNumber second operand.
     * @return the power's result.
     */
    public static double power(double firstNumber, int secondNumber) {
        double result = firstNumber;

        if(secondNumber == 0) {
            return 1;
        }

        if(secondNumber > 0) {
            for(int index = 2; index <= secondNumber; index++) {
                result = result * firstNumber;
            }
        }
        else if(secondNumber < 0) {
            for(int index = -2; index >= secondNumber; index--) {
                result = result * firstNumber;
            }

            result = 1 / result;
        }

        return result;
    }
}