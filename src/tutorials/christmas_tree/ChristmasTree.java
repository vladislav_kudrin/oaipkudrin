package tutorials.christmas_tree;

/**
 *
 *
 *
 *
 * @author nxfh
 */
public class ChristmasTree {
    private double height;
    private boolean natural;

    public ChristmasTree(double height, boolean natural) {
        this.height = height;
        this.natural = natural;
    }

    /**
     * A method calculates a christmas tree price.
     * @return the price.
     */
    public double calculatePrice() {
        if(natural) {
            return height * 1495;
        }
        else {
            return height * 1920;
        }
    }

    /**
     * A method calculates a recommended natural christmas tree for a current balance.
     * @param balance the current balance.
     * @return a result.
     */
    public static ChristmasTree showValidNatural(double balance) {
        return new ChristmasTree(balance / 1495, true);
    }

    /**
     * A method calculates a recommended not natural christmas tree for thr current balance.
     * @param balance the current balance.
     * @return a result.
     */
    public static ChristmasTree showValidNotNatural(double balance) {
        return new ChristmasTree(balance / 1920, false);
    }

    public double getHeight() {
        return height;
    }

    public String toString() {
        return "Height: " + height + " metres" +
                "\nNatural: " + (natural ? "yes" : "no");
    }
}