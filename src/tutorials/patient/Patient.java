package tutorials.patient;

public class Patient {
    private String surname;
    private int birthYear;
    private int cardNumber;
    private boolean medicalExam;

    public Patient(String surname, int birthYear, int cardNumber, boolean medicalExam) {
        this.surname = surname;
        this.birthYear = birthYear;
        this.cardNumber = cardNumber;
        this.medicalExam = medicalExam;
    }

    public Patient() {
        this("Not stated", 0, 0, false);
    }

    public String getSurname() {
        return surname;
    }

    public int getBirthYear() {
        return birthYear;
    }

    public int getCardNumber() {
        return cardNumber;
    }

    public boolean isMedicalExam() {
        return medicalExam;
    }

    public String toString() {
        return "Patient{" +
                "surname: " + surname +
                "; year of birth: " + birthYear +
                "; card's number: " + cardNumber +
                "; medical examination: " + ((medicalExam)?"completed":"not completed") +
                '}';
    }
}