package tutorials.song;

import java.util.Scanner;

public class Demo {
    public static Scanner input = new Scanner(System.in);

    public static void main(String[] args) {
        double duration;
        Song[] songs = new Song[5];

        songs[0] = new Song("Inside of every demon is a rainbow", "Charlie", 180);
        songs[1] = new Song("I'm always chasing rainbows", "Charlie", 140);
        songs[2] = new Song("Alastor's reprice", "Alastor", 60);
        songs[3] = new Song("This is America", "BlackMan", 240);
        songs[4] = new Song("All star", "Shrek", 210);

        System.out.print("Please, enter a song's duration as minutes: ");
        duration = input.nextDouble() * 60;

        compareDuration(songs, duration);

        Song.findShortSongs(songs);

        System.out.print("A category of a first song" + (Song.compareCategories(songs[0], songs[songs.length - 1])?"is ":"isn't "));
        System.out.println("equal to a category of a last song.");
    }

    private static void compareDuration(Song [] songs, double duration) {
        int counter = 0;

        System.out.println("\nSongs with a current duration:");

        for(int index = 0; index < songs.length; index++) {
            if(songs[index].getSeconds() == duration) {
                System.out.println(songs[index]);
                counter++;
            }
        }

        if(counter < 1) {
            System.out.println("Not found");
        }
    }
}