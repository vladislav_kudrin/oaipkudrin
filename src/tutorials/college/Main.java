package tutorials.college;

import java.util.ArrayList;

public class Main {
    public static void main(String[] args) {
        ArrayList<Student> students = new ArrayList<Student>();
        ArrayList<Teacher> teachers = new ArrayList<Teacher>();
        ArrayList<Person> persons = new ArrayList<Person>();

        students.add(new Student("Grishkin", Gender.MALE, 2018, "18"));
        students.add(new Student("Mirnova", Gender.FEMALE, 2016, "19"));
        students.add(new Student("Kudrinov", Gender.MALE, 2018, "18"));
        students.add(new Student("Semyonkina", Gender.FEMALE, 2017, "20"));
        students.add(new Student("Shumilkin", Gender.MALE, 2017, "21"));

        teachers.add(new Teacher("Vorobyovin", Gender.MALE, "Math", true));
        teachers.add(new Teacher("Kudryashovina", Gender.FEMALE, "History", false));
        teachers.add(new Teacher("Zolotova", Gender.FEMALE, "Economy", false));
        teachers.add(new Teacher("Adelshinova", Gender.FEMALE, "Russian", true));
        teachers.add(new Teacher("Silachov", Gender.MALE, "Physical culture", true));

        for(int index = 0; index < students.size(); index++) {
            persons.add(teachers.get(index));
            persons.add(students.get(index));
        }

        System.out.println("Female students who enrolled in a College in 2017:");
        Student.showFemales(students);
        System.out.println("\nCurators: ");
        Teacher.showCurators(teachers);
        System.out.println("\nMale students and teachers: ");
        Person.showMales(persons);
    }
}
