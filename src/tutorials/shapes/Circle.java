package tutorials.shapes;

public class Circle extends Shape{
    private Point center;
    private double radius;

    public double area() {
        return Math.PI * radius * radius;
    }

    Circle(Color color, Point center, double radius) {
        super(color);
        this.center = center;
        this.radius = radius;
    }

    public String toString() {
        return "Circle{" +
                "color=" + getColor() +
                ", center=" + center +
                ", radius=" + radius +
                '}';
    }
}