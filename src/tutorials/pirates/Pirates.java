package tutorials.pirates;

public class Pirates {
    public static void main(String[] args) {
        int gems = 0;

        gems = calculateGemsNumber(gems);
        System.out.println("Minimum gems: " + gems + " gems.");
        gems = calculateGemsNumber(gems);
        System.out.println("Other value: " + gems + " gems.");
    }

    private static int calculateGemsNumber(int gems) {
        boolean flag = true;

        do {
            gems+=7;
            flag = true;

            for(int pirates = 2; pirates < 7; pirates++) {
                flag = gems % pirates != 1 ? false : flag;
            }
        }
        while (!flag);

        return gems;
    }
}