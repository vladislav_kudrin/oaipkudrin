package tutorials.shapes;

public class Square extends Shape{
    private Point corner;
    private double side;

    public double area() {
        return side * side;
    }

    Square(Color color, Point corner, double side) {
        super(color);
        this.corner = corner;
        this.side = side;
    }

    public String toString() {
        return "Circle{" +
                "color=" + getColor() +
                ", corner=" + corner +
                ", side=" + side +
                '}';
    }
}