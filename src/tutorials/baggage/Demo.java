package tutorials.baggage;

import java.util.Scanner;

public class Demo {
    public static Scanner input = new Scanner(System.in);

    public static void main(String[] args) {

        System.out.print("Please, enter a number of passengers: ");
        Baggage[] baggage = new Baggage[input.nextInt()];

        inputCharacteristics(baggage);

        System.out.println("\nPassengers with a hand baggage:");

        for(int index = 0; index < baggage.length; index++) {
            if(Baggage.defineHandLuggage(baggage[index])) {
                System.out.println(baggage[index].isSurname());
            }
        }
    }

    private static void inputCharacteristics(Baggage[] baggage) {
        String surname;
        int space;
        double weight;

        for(int index = 0; index < baggage.length; index++) {
            System.out.println("\nPlease, enter characteristics for a passenger " + (index + 1));
            System.out.print("Please, enter a passenger's surname: ");
            input.nextLine();
            surname = input.nextLine();
            System.out.print("Please, enter a number of baggage's space: ");
            space = input.nextInt();
            System.out.print("Please, enter a baggage's weight: ");
            weight = input.nextDouble();

            baggage[index] = new Baggage(surname, space, weight);
        }
    }
}