package tutorials.country;

public class Country {
    private String name;
    private String capital;
    private double square;
    private int population;

    public Country(String name, String capital, double square, int population) {
        this.name = name;
        this.capital = capital;
        this.square = square;
        this.population = population;
    }

    public Country() {
        this("Unknown", "Unknown", 0, 0);
    }

    public String getName() {
        return name;
    }

    public String getCapital() {
        return capital;
    }

    public double getSquare() {
        return square;
    }

    public int getPopulation() {
        return population;
    }

    public double density() {
        return population / square;
    }

    public String toString() {
        return name +
                " {" +
                "capital: " + capital +
                "; square: " + square +
                "; population: " + population +
                "}\n";
    }
}