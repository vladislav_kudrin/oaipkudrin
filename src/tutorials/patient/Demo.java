package tutorials.patient;

public class Demo {
    public static void main(String[] args) {
        Patient[] patients = new Patient[5];

        patients[0] = new Patient("Иванов", 1999, 1, true);
        patients[1] = new Patient("Пупкин", 1979, 2, false);
        patients[2] = new Patient("Семёнов", 1985, 3, true);
        patients[3] = new Patient("Гришин", 2002, 4, false);
        patients[4] = new Patient("Кудрин", 2001, 5, true);

        showCompleted(patients);
    }

    private static void showCompleted(Patient[] patients) {
        for(int index = 0; index < patients.length; index++) {
            if(patients[index].isMedicalExam() && patients[index].getBirthYear() < 2000) {
                System.out.println(patients[index]);
            }
        }
    }
}