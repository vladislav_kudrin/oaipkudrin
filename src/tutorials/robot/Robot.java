package tutorials.robot;

public class Robot {
    private int coordX;
    private int coordY;
    private Direction direction;

    public Robot(int coordX, int coordY, Direction direction) {
        this.coordX = coordX;
        this.coordY = coordY;
        this.direction = direction;
    }

    public int getCoordX() {
        return coordX;
    }

    public int getCoordY() {
        return coordY;
    }

    public Direction getDirection() {
        return direction;
    }

    public void setCoordX(int coordX) {
        this.coordX = coordX;
    }

    public void setCoordY(int coordY) {
        this.coordY = coordY;
    }

    public void setDirection(Direction direction) {
        this.direction = direction;
    }
}