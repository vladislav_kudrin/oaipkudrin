package tutorials.sea_battle;

import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);

        int field = (int) (Math.random() * 7 + 4);
        int shipLength = (int) (Math.random() * 4 + 1);
        int startCoord = (int) (Math.random() * (field - shipLength + 1) + 1);
        int coord;
        String result;

        Ship ship = new Ship(startCoord, shipLength);

        System.out.println("Field size: " + field + "\nShip length: " + shipLength);

        do {
            System.out.print("Please, enter your coordinate: ");
            coord = input.nextInt();

            result = ship.shoot(coord);
            System.out.println(result);
        }
        while(!result.equals("You won! Ship is defeated and drowned!"));
    }
}